# MASTER CLOCK
live_loop :masterclock do
  use_bpm 130
  cue :tick
  midi_clock_tick
  sleep 1
end
define :bell do |n, r|
  detune = 0.1
  synth :square, note: n, release: r
  synth :square, note: n + detune, amp: 2, release: r
  synth :gnoise, release: r, amp: 1, cutoff: 60
  synth :gnoise, release: r / 4, amp: 1, cutoff: 100
  synth :noise, release: r / 8, amp: 1, cutoff: 90
end


bell :e1, 0.15
sleep 1
bell :f1, 0.25

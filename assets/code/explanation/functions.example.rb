define :bell do
  detune = 0.1
  synth :square, note: :e1, release: 0.25
  synth :square, note: :e1 + detune, amp: 2, release: 0.25
  synth :gnoise, release: 0.25, amp: 1, cutoff: 60
  synth :gnoise, release: 0.25 / 4, amp: 1, cutoff: 100
  synth :noise, release: 0.25 / 8, amp: 1, cutoff: 90
end


bell
sleep 1
bell

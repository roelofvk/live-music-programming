# @Author: RoelofVK
# @Date:   2018-01-11T00:19:21+01:00
# @Email:  imkitsun@gmail.com
# Link: https://roelofvk.gitlab.io/live-music-programming/

samples = "F:\\DEV\\sonic pi\\Presentation\\assets\\samples\\", "DRUM"
use_bpm 145

live_loop :break do
  rew = [true, true, true, true, false].choose
  
  sample samples, "LOOP1",
    beat_stretch: 8,
    amp: 4,
    hpf: 100,
    rate: rew ? -1 : 1,
    slice: rew ? [1, 6, 2, 7].choose : nil
  
  wait rew ? 0.25 : 8
end
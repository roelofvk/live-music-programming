# Welcome to Sonic Pi v2.11.1

use_synth :tb303
use_debug false
use_bpm 65

samples = "F:\\DEV\\sonic pi\\Presentation\\assets\\samples\\", "909"

# MASTER CLOCK
live_loop :masterclock do
  cue :tick
  sleep 0.5
end


live_loop :kick, sync: :tick do
  sample samples, "KICK", amp: 4
  wait 0.5  
end


live_loop :snare,sync: :tick do
  wait 1
  sample samples, "CLAP", amp: 1
end


live_loop :cymbals, sync: :tick do
  # sample samples, "RIDE", amp: 0.1,
  # decay: 1,
  # sustain_level: 0
  wait 0.25
  
  sample samples, "RIDE", amp: 0.3,
    decay: 0.125,
    sustain_level: 0
  wait 0.25
end


live_loop :squelch, sync: :tick do
  use_random_seed 3000
  with_fx :reverb do
    16.times do
      # n = (ring :e1, :e2, :e3, [:b2, :fs2, :b1].choose, [:f2, :g2, :b1].choose).tick
      n = (ring :e1, :e2, :e3).tick
      play n,
        release: 0.125,
        cutoff: rrand(50, 80),
        wave: 1,
        amp: 1,
        pan: rrand(-0.25, 0.25),
        res: 0.9
      sleep 0.125
    end
  end
end

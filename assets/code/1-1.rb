# @Author: RoelofVK
# @Date:   2018-01-08T12:43:18+01:00
# @Email:  imkitsun@gmail.com

use_synth :chiplead
use_bpm 400

play :e5
play :Fs4
play :d3
wait 1

play :e5
play :Fs4
play :d3
wait 2

play :e5
play :Fs4
play :d3
wait 2

play :c5
play :Fs4
play :d3
wait 1

play :e5
play :Fs4
play :d3
wait 2

play :g5
play :b4
play :g4
wait 4

play :g4
play :g3
wait 4


# PLAY MAIN MELODY
loop do
  2.times do
    play :c5
    play :e4
    play :g3
    wait 3
    
    play :g4
    play :c4
    play :e3
    wait 3
    
    play :e4
    play :g3
    play :c2
    wait 3
    
    play :a4
    play :c4
    play :f3
    wait 2
    
    play :b4
    play :d4
    play :g3
    wait 2
    
    play :bb4
    play :db4
    play :gb3
    wait 1
    
    play :a4
    play :c4
    play :f3
    wait 2
    
    play :g4
    play :c4
    play :e3
    wait 4/3
    
    play :e5
    play :g4
    play :c4
    wait 4/3
    
    play :g5
    play :b4
    play :e4
    wait 4/3
    
    play :a5
    play :c5
    play :f4
    wait 2
    
    play :f5
    play :a4
    play :d4
    wait 1
    
    play :g5
    play :b4
    play :e4
    wait 2
    
    play :e5
    play :a4
    play :c4
    wait 2
    
    play :c5
    play :e4
    play :a3
    wait 1
    
    play :d5
    play :f4
    play :b3
    wait 1
    
    play :b4
    play :d4
    play :g3
    wait 3
  end
  2.times do
    play :c3
    wait 2
    
    play :g5
    play :e5
    wait 1
    
    play :gb5
    play :eb5
    play :g3
    wait 1
    
    play :f5
    play :d5
    wait 1
    
    play :Ds5
    play :b4
    wait 1
    
    play :c4
    wait 1
    
    play :e5
    play :c5
    wait 1
    
    play :f3
    wait 1
    
    play :gs4
    play :e4
    wait 1
    
    play :a4
    play :f4
    wait 1
    
    play :c5
    play :g4
    play :c4
    wait 1
    
    play :c4
    wait 1
    
    play :a4
    play :c4
    wait 1
    
    play :c5
    play :e4
    play :f3
    wait 1
    
    play :d5
    play :f4
    wait 1
    
    play :c3
    wait 2
    
    play :g5
    play :e5
    wait 1
    
    play :gb5
    play :eb5
    play :e3
    wait 1
    
    play :f5
    play :d5
    wait 1
    
    play :Ds5
    play :b4
    wait 1
    
    play :g3
    wait 1
    
    play :e5
    play :c5
    play :c4
    wait 2
    
    play :c6
    play :g5
    play :f5
    wait 2
    
    play :c6
    play :g5
    play :f5
    wait 1
    
    play :c6
    play :g5
    play :f5
    wait 2
    
    play :g3
    wait 2
    
    play :c3
    wait 2
    
    play :g5
    play :e5
    wait 1
    
    play :gb5
    play :eb5
    play :g3
    wait 1
    
    play :f5
    play :d5
    wait 1
    
    play :Ds5
    play :b4
    wait 1
    
    play :c4
    wait 1
    
    play :e5
    play :c5
    wait 1
    
    play :f3
    wait 1
    
    play :gs4
    play :e4
    wait 1
    
    play :a4
    play :f4
    wait 1
    
    play :c5
    play :g4
    play :c4
    wait 1
    
    play :c4
    wait 1
    
    play :a4
    play :c4
    wait 1
    
    play :c5
    play :e4
    play :f3
    wait 1
    
    play :d5
    play :f4
    wait 1
    
    play :c3
    wait 2
    
    play :eb5
    play :ab4
    play :ab3
    wait 3
    
    play :d5
    play :f4
    play :Bb3
    wait 3
    
    play :c5
    play :e4
    play :c4
    wait 3
    
    play :g3
    wait 1
    
    play :g3
    wait 2
    
    play :c3
    wait 2
    
  end
  
end

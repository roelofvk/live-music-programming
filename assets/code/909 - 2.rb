samples = "C:\\DEV\\sonicpi\\pres\\assets\\samples", "909"

live_loop :master_clock do
  use_bpm 140
  cue :tick
  wait 1
end

live_loop :kick, sync_bpm: :tick do
  sample samples, "KICK",
    amp: 4,
    lpf: 130,
    rate: 1
  wait 1
end

live_loop :ride, sync_bpm: :tick do
  wait 0.5
  sample samples, "RIDE",
    amp: 0,
    release: 0.25,
    decay: [0.5, 0.25].choose,
    pan: rrand(-0.5, 0.5),
    sustain_level: 0
  wait 0.5
  
end

live_loop :clap, sync_bpm: :tick do
  
  wait 1
  sample samples, "CLAP",
    amp: 0
  wait [1].choose
  
  
end


live_loop :arp, sync_bpm: :tick do
  use_synth :dull_bell
  with_fx :reverb do
    [:e1, :e2, :e3].ring.each do |n|
      play n,
        release: 0.25,
        cutoff_decay: 1.25,
        cutoff: rrand(0, 130),
        amp: 4,
        res: 0.9
      wait [0.25].choose
    end
  end
  
end


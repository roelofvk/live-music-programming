// Import React
import React from "react";

// Import Spectacle Core tags
import {
  BlockQuote,
  Cite,
  Deck,
  Heading,
  ListItem,
  List,
  Quote,
  Slide,
  Text,
  CodePane,
  Image,
  Appear,
  Layout,
  Markdown,
  Link
} from "spectacle";

// Import theme
import createTheme from "spectacle/lib/themes/default";
import 'prismjs/themes/prism.css';

import CodeSlide from 'spectacle-code-slide';

// Require CSS
require("normalize.css");

const theme = createTheme({
  primary: "white",
  secondary: "#1F2022",
  tertiary: "#03A9FC",
  quarternary: "#CECECE"
}, {
  primary: "Montserrat",
  secondary: "Helvetica"
});

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck transition={["slide"]} transitionDuration={500} theme={theme}>

        <Slide transition={["zoom"]} bgColor="tertiary">
          <Heading size={2} fit caps lineHeight={1} textColor="primary">
            Muziek programmeren als vorm van performance
          </Heading>

        </Slide>




        <Slide transition={["fade"]} bgColor="primary">
          {/* WAAROM? */}
          <Heading>But why?</Heading>
          <List>
            <ListItem>Uitdaging</ListItem>
            <ListItem>Nieuwe vorm van performance</ListItem>
            <ListItem>voor art dude, voor art</ListItem>
          </List>
        </Slide>




        <Slide transition={["fade"]} bgColor="primary" textColor="secondary">
          {/* VOORBEELDEN  */}
          <Heading color='tertiary'>Voorbeeld</Heading>
          <Layout>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/KJPdbp1An2s?rel=0&amp;controls=0&amp;showinfo=0&amp;start=1380" frameBorder="0" gesture="media" allow="encrypted-media" allowFullScreen />
            <iframe width="560" height="315" src="https://www.youtube.com/embed/cydH_JAgSfg?rel=0&amp;controls=0&amp;showinfo=0&amp;start=1380" frameBorder="0" gesture="media" allow="encrypted-media" allowFullScreen />
          </Layout>
        </Slide>



        <Slide align="center flex-start" transition={["fade"]} bgColor="primary" textColor="primary">
          {/* SONIC PI */}
          <Image width={300} src={require('../assets/images/logo_spi.png')} />
          <Heading>Sonic pi</Heading>
          <Text>Open source</Text>
          <Text>"Makkelijk"</Text>
          <Link href="http://sonic-pi.net/">http://sonic-pi.net/</Link>
        </Slide>


        <Slide bgColor="tertiary" textColor='secondary'>
          {/* IN HET PROGRAMMA SYNTS + SAMPLES  */}
          <Heading textColor="secondary">Basics</Heading>
          <Text>Noten</Text>
          <Text>Samples</Text>
          <Text>Variabelen</Text>
        </Slide>

        <Slide bgColor="tertiary" textColor='secondary'>
          {/* IN HET PROGRAMMA SYNTS + SAMPLES  */}
          <Text>Noten</Text>
          <CodePane
            lang="ruby"
            source={require('raw-loader!../assets/code/explanation/play.example.rb')}
          />
        </Slide>

        <Slide bgColor="tertiary" textColor='secondary'>
          {/* IN HET PROGRAMMA SYNTS + SAMPLES  */}
          <Text>Samples</Text>
          <CodePane
            lang="ruby"
            source={require('raw-loader!../assets/code/explanation/samples.example.rb')}
          />
        </Slide>


        <Slide bgColor="tertiary" textColor='secondary'>
          {/* IN HET PROGRAMMA SYNTS + SAMPLES  */}
          <Text>Variabelen</Text>
          <CodePane
            lang="ruby"
            source={require('raw-loader!../assets/code/explanation/variables.example.rb')}
          />

          <Appear>
            <CodePane
              lang="ruby"
              source={require('raw-loader!../assets/code/explanation/variables.2.example.rb')}
            />
          </Appear>
          <Appear>
            <CodePane
              lang="ruby"
              source={require('raw-loader!../assets/code/explanation/variables.3.example.rb')}
            />
          </Appear>
        </Slide>



        <Slide transition={["fade"]} bgColor="primary" textColor="primary">
          {/* ARRAY */}
          <Heading>Arrays</Heading>
          <Text>Een array is een lijst met informatie</Text>
          <Appear>
            <CodePane
              lang="ruby"
              source={require('raw-loader!../assets/code/explanation/arrays.example.rb')}
            />
          </Appear>



        </Slide>

        <Slide>

          <Text>Voor elke waarde in een array een stuk code uitvoeren</Text>
          <CodePane
            lang="ruby"
            source={require('raw-loader!../assets/code/explanation/arrays.2.example.rb')}
          />

        </Slide>



        <Slide transition={["fade"]} bgColor="primary" textColor="primary">
          {/* LOOP */}
          <Heading>Looping</Heading>
          <CodePane
            lang="ruby"
            source={require('raw-loader!../assets/code/explanation/loops.example.rb')}
          />

          <Appear>
            <CodePane
              lang="ruby"
              source={require('raw-loader!../assets/code/explanation/loops.2.example.rb')}
            />
          </Appear>
        </Slide>

        {/* IN HET PROGRAMMA ARRAY LOOP */}

        <Slide transition={["fade"]} bgColor="primary" textColor="primary">
          {/* LIVE LOOPS */}
          <Heading>live loops</Heading>
          <CodePane
            lang="ruby"
            source={require('raw-loader!../assets/code/explanation/liveloops.example.rb')}
          />
        </Slide>



        <Slide transition={["fade"]} bgColor="primary" textColor="primary">
          {/* LIVE LOOPS */}
          <Heading>Effects</Heading>
          <CodePane
            lang="ruby"
            source={require('raw-loader!../assets/code/explanation/fx.example.rb')}
          />
        </Slide>



        {/* VOORBEELD MET CODE */}
        {/* <CodeSlide
          bgColor='primary'
          textColor='secondary'
          transition={[]}
          lang="ruby"
          code={require("raw-loader!../assets/code/909.rb")}
          ranges={[
            { loc: [2, 3], note: 'Set synth' },
            { loc: [4, 5], note: "Set BPM" },
            { loc: [8, 13], note: 'setup master clock' },
            { loc: [10, 11] },
            { loc: [15, 16] },
            { loc: [6, 7], note: "Select the sample directory" },
            { loc: [15, 19], note: "Set up kick loop" }
            // ...
          ]}
        /> */}

        <CodeSlide
          bgColor='primary'
          textColor='secondary'
          transition={[]}
          lang="ruby"
          // align="center flex-start"
          code={require("raw-loader!../assets/code/masterclock.rb")}
          ranges={[
            { loc: [1, 7], note: 'Maak een liveloop (het liefst in een aparte buffer)' },
            { loc: [2, 3], note: "Set bpm" },
            { loc: [3, 4], note: "Send out cue to sync loops to" },
            { loc: [4, 5], note: "Send out midi clock to all  connected midi devices" },
            { loc: [5, 6], note: "Wait 1 quarter note" }
          ]}
        />

        {/** BREAK */}
        <CodeSlide
          bgColor='primary'
          textColor='secondary'
          transition={[]}
          lang="ruby"
          // align="center flex-start"
          code={require("raw-loader!../assets/code/IDMBreak.rb")}
          ranges={[
            { loc: [5, 6], note: 'Load up samples' },
            { loc: [6, 7], note: "Set BPM" },
            { loc: [8, 20] },
            { loc: [9, 10], note: 'Choose random true or false to use for rewind effect' },
            { loc: [11, 17], note: 'Play sample' },
            { loc: [12, 13], note: "Stretch sample to fit exactly 8 beats" },
            { loc: [13, 14], note: "Set sample amplitude" },
            { loc: [14, 15], note: "Set a highpass filter" },
            { loc: [15, 16], note: "Based on the true false play the sample backwards" },
            { loc: [16, 17], note: "Take one 16th of the sample and use that for the rewind" },
            { loc: [18, 19], note: "Based on the rewind effect, wait short or wait for the loop to finish" }
          ]}
        />

        <Slide>
          <Heading>Functions</Heading>
          <Text>Functions zijn stukken code die je kunt hergebruiken </Text>
          <CodePane
            lang="ruby"
            source={require('raw-loader!../assets/code/explanation/functions.example.rb')}
          />
        </Slide>
        <Slide>
          <Text>je kan functies maken met variabelen per run van de functie</Text>
          <CodePane
            lang="ruby"
            source={require('raw-loader!../assets/code/explanation/functions.2.example.rb')}
          />
        </Slide>


        <Slide transition={["fade"]} bgColor="tertiary" textColor="secondary">
          {/* LIVE LOOPS */}
          <Heading textColor='primary'>Ending</Heading>
          <Text><Link href="https://roelofvk.gitlab.io/live-music-programming">Presentatie</Link></Text>
          <Text><Link href="https://gitlab.com/roelofvk/live-music-programming/">Code voorbeelden</Link></Text>
        </Slide>

      </Deck>
    );
  }
}
